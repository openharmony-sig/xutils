/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export class LruDiskCacheBean {
    title: string = null;
    cacheVersion: string = null;
    appVersion: string = null
    maxValue: number = 0
    storageSize: number
    imageKey: Array<string> = new Array<string>()

    constructor(json: string) {
        var myObject = JSON.parse(json);

        this.title = myObject['title']
        this.cacheVersion = myObject['cacheVersion']
        this.appVersion = myObject['appVersion']
        this.maxValue = myObject['maxValue']
        this.storageSize = myObject['storageSize']
        this.imageKey = myObject['imageKey']
    }

    setTitle(title: string) {
        this.title = title;
    }

    getTitle(): string {
        return this.title
    }

    setCacheVersion(cacheVersion: string) {
        this.cacheVersion = cacheVersion;
    }

    getCacheVersion(): string {
        return this.cacheVersion
    }

    setAppVersion(appVersion: string) {
        this.appVersion = appVersion;
    }

    getAppVersion(): string {
        return this.appVersion
    }

    setMaxValue(maxValue: number) {
        this.maxValue = maxValue;
    }

    getMaxValue(): number {
        return this.maxValue
    }

    setImageKey(imageKey: string) {
        this.imageKey.push(imageKey);
    }

    getImageKey(): Array<string> {
        return this.imageKey
    }

    setStorageSize(storageSize: number) {
        this.storageSize = storageSize
    }

    getStorageSize(): number {
        return this.storageSize
    }

    deleteItemImageKey(value: string) {
        let DeleteIndex = this.imageKey.indexOf(value)
        if (DeleteIndex >= 0) {
            this.imageKey.splice(DeleteIndex, 1)
        }
    }

    deleteAllImageKey() {
        this.imageKey.splice(0, this.imageKey.length)
        return
    }

    toJson(): string {
        let result: string
        if (this.imageKey.length == 0) {
            result = '{"title": "libcore.io.DiskLruCache","cacheVersion": "1","appVersion": "' + this.getAppVersion() + '","maxValue": ' + this.getMaxValue() + ',"storageSize":' + this.getStorageSize() + ',"imageKey":[]}';
        } else {
            result = '{"title": "libcore.io.DiskLruCache","cacheVersion": "1","appVersion": "' + this.getAppVersion() + '","maxValue": ' + this.getMaxValue() + ',"storageSize":' + this.getStorageSize()
            + ',"imageKey":[';
            for (var index = 0; index < this.imageKey.length; index++) {
                result = result + '"' + this.imageKey[index] + '"';
                if (index < this.imageKey.length - 1) {
                    result = result + ','
                }
            }
            result = result + ']}'
        }
        return result

    }
}