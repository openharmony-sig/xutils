/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Priority } from '../task/Priority'

export class RequestParams {
    private headers: Map<string, any> ;
    private queryStringParams: Map<string, any> ;
    private priority: Priority ;

    public getPriority(): Priority {
        return this.priority;
    }

    public setPriority(priority: Priority) {
        this.priority = priority;
    }

    public addHeader(name: string, value: string) {
        if (this.headers == null) {
            this.headers = new Map()
        }
        this.headers.set(name, value)
    }

    public addQueryStringParameter(name: string, value: string) {
        if (this.queryStringParams == null) {
            this.queryStringParams = new Map()
        }
        this.queryStringParams.set(name, value)
    }

    public getQueryStringParams(): Map<string, any> {
        return this.queryStringParams;
    }

    public getHeaders(): Map<string, any> {
        return this.headers;
    }
}