/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ResponseInfo } from '../ResponseInfo'

export abstract class RequestCallBack<T> {
    private DEFAULT_RATE = 1000;
    private MIN_RATE = 200;
    private requestUrl: string;
    private userTag: ESObject;
    private rate: number;

    public getRate(): number {
        if (this.rate < this.MIN_RATE) {
            return this.MIN_RATE;
        }
        return this.rate;
    }

    public setRate(rate: number) {
        this.rate = rate;
    }

    public onStart() {
    }

    public onCancelled() {
    }

    public abstract onSuccess(responseInfo: ResponseInfo<T>);

    public abstract onFailure();

    public abstract onError(err: ESObject);

    public abstract onDownloadProgress(receivedSize: number, totalSize: number)

    public abstract onDownloadSuccess();

    public abstract onProgress(uploadedSize:number,totalSize:number);
}
