/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import { BitmapDisplayConfig } from '../BitmapDisplayConfig';
import { BitmapSetter } from './BitmapSetter';

export abstract class BitmapLoadCallBack {


    /**
     * Call back when start loading.
     *
     * @param container
     * @param uri
     * @param config
     */

    bitmapSetter: BitmapSetter;

    onLoadStarted(uri: string, config: BitmapDisplayConfig) {

    }

    /**
     * Call back when loading.
     *
     * @param container
     * @param uri
     * @param config
     * @param total
     * @param current
     */
    onLoadingPixelMap(PixelMap: PixelMap) {
    }

    onLoadingResource(Resource: Resource) {
    }

    onLoadMemoryCompleted(url: string, pixelMap: PixelMap) {
    }

    /**
     * Call back when bitmap has loaded.
     *
     * @param container
     * @param uri
     * @param bitmap
     * @param config
     */
    abstract onLoadCompleted(url: string, pixelMap: PixelMap)

    abstract onLoadDiskCompleted(url: string, pixelMap: PixelMap)

    /**
     * Call back when bitmap failed to load.
     *
     * @param container
     * @param uri
     * @param drawable
     */
    abstract onLoadFailedPixelMap(mFailedPixelMap: PixelMap)

    abstract onLoadFailedResource(Resource: Resource)

    setBitmapSetter(bitmapSetter: BitmapSetter) {
        this.bitmapSetter = bitmapSetter;
    }

    setDrawable(pixelMap: PixelMap) {
        if (this.bitmapSetter != null) {
            this.bitmapSetter.setPixelMap(pixelMap);
        }
    }

    abstract onProgressUpdate(receivedSize: number, totalSize: number)
}
