/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import prompt from '@system.prompt';
import fileIo from '@ohos.file.fs';
import { HttpUtils } from '@ohos/xutils/'
import { HttpMethod } from '@ohos/xutils/'
import { RequestCallBack } from '@ohos/xutils/'
import { RequestParams } from '@ohos/xutils/'
import { ResponseInfo } from '@ohos/xutils/'
import { DbUtils } from '@ohos/xutils/'
import { DaoConfig } from '@ohos/xutils/'
import { QueryCallBack } from '@ohos/xutils/'
import { Selector } from '@ohos/xutils/'
import { BitmapUtils } from '@ohos/xutils/'
import { BitmapLoadCallBack } from '@ohos/xutils/'
import { BitmapDisplayConfig } from '@ohos/xutils/'
import { User } from '../entity/User';
import { Weather } from '../entity/Weather'
import { DB_NAME, TABLE_NAME, SQL_CREATE_TABLE, COLUMNS } from '../entity/RdbConfig'
import { valuesBuckets, newValuesBuckets } from '../entity/Employee'

export class FileBean {
  filename: string = ''
  name: string = ''
  uri: string = ''
  type: string = ''
}

export class DataBean {
  name: string = ''
  value: string = ''
}

export class UploadBean {
  url: string = ''
  method: HttpMethod = HttpMethod.POST
  header: Map<string, Object> = new Map<string, Object>()
  files: FileBean[] =[]
  data: DataBean[] =[]
}

@Entry
@Component
struct Index {
  @State pixelMap: PixelMap | null = null;
  @State diskPixMap: PixelMap | null = null;
  private context: Context = getContext()
  db: DbUtils | null = null
  mBitmapUtils: BitmapUtils = new BitmapUtils(this.context);
  config: DaoConfig = new DaoConfig()
  start: boolean = false
  build() {
    Scroll() {
      Column() {
        Text($r('app.string.GET_request'))
          .fontSize(20)
          .margin(30)
          .fontWeight(FontWeight.Bold).onClick(() => {
          new HttpUtils()
            .send(HttpMethod.GET,
              "http://hshapp.ncn.com.cn/wisdom3/config/config.do",
              new HttpCallBack());
        })
        Text($r('app.string.POST_request'))
          .fontSize(20)
          .margin(30)
          .fontWeight(FontWeight.Bold).onClick(() => {
          let requestParams: RequestParams = new RequestParams();
          requestParams.addHeader("Content-Type", "application/json");
          requestParams.addQueryStringParameter("user_id", "108913");
          new HttpUtils()
            .sendParams(HttpMethod.POST,
              "http://hshapp.ncn.com.cn/wisdom3/config/config.do",
              requestParams,
              new HttpCallBackPost())
        })
        Text($r('app.string.Upload_a_file'))
          .fontSize(20)
          .margin(30)
          .fontWeight(FontWeight.Bold)
          .onClick(() => {
            // 文件上传成功后的路径  http://106.15.92.248/node/uploads/
            this.upload()
          })
        Text($r('app.string.Download_a_file'))
          .fontSize(20)
          .margin(30)
          .fontWeight(FontWeight.Bold).onClick(() => {
          //   文件下载的路径在 data/app/el2/100/base/com.ohos.xutils/haps/entry/files 目录下
          new HttpUtils().download('https://5b0988e595225.cdn.sohucs.com/images/20200206/3ed7fb3b096f4c2b9d66bd65baaa6c0e.jpeg',
            '/hsh.jpeg', new DownloadCallBack())
        })
        Divider().margin({ top: 10 })
        Text($r('app.string.Create_a_database'))
          .fontSize(20)
          .margin(30)
          .fontWeight(FontWeight.Bold).onClick(() => {
          this.config.setDbName(DB_NAME)
          this.db = new DbUtils(this.config)
          this.db.setCallback(new DbCallBack())
        })
        Text($r('app.string.Create_a_table'))
          .fontSize(20)
          .margin(30)
          .fontWeight(FontWeight.Bold).onClick(() => {
          this.config.setDbName(DB_NAME)
          this.config.setTableName(TABLE_NAME)
          this.config.setCreateTableSql(SQL_CREATE_TABLE)
          if (this.db) this.db.createTableIfNotExist();
        })
        Text($r('app.string.Insert_data'))
          .fontSize(20)
          .margin(30)
          .fontWeight(FontWeight.Bold).onClick(() => {
          if (this.db) this.db.saveAll(valuesBuckets);
        })
        Text($r('app.string.Query_all_data_sets'))
          .fontSize(20)
          .margin(30)
          .fontWeight(FontWeight.Bold).onClick(() => {
          if (this.db) this.db.findAll(Selector.from(TABLE_NAME, COLUMNS));
        })
        Text($r('app.string.Query_data_sets_that_meet_the_specified_criteria'))
          .fontSize(20)
          .margin(30)
          .fontWeight(FontWeight.Bold).onClick(() => {
          if (this.db) this.db.execNonQuery("",Selector.from(TABLE_NAME, COLUMNS)
            .where("NAME", "equalTo", "Lisa")
            .and("AGE", "equalTo", 18),"query");
        })
        Text($r('app.string.Query_the_first_data_record_that_meets_the_specified_criteria'))
          .fontSize(20)
          .margin(30)
          .fontWeight(FontWeight.Bold).onClick(() => {
          if (this.db) this.db.execNonQuery("",Selector.from(TABLE_NAME, COLUMNS).where("NAME", "equalTo", "Rose"),"queryFirst");
        })
        Text($r('app.string.Update_data'))
          .fontSize(20)
          .margin(30)
          .fontWeight(FontWeight.Bold).onClick(() => {
          if (this.db) this.db.update(newValuesBuckets[0], Selector.from(TABLE_NAME, COLUMNS)
            .where("NAME", "equalTo", "Rose"));
        })
        Text($r('app.string.Delete_data'))
          .fontSize(20)
          .margin(30)
          .fontWeight(FontWeight.Bold).onClick(() => {
          if (this.db) this.db.delete(Selector.from(TABLE_NAME, COLUMNS)
            .where("NAME", "equalTo", "Rose"));
        })
        Text($r('app.string.Delete_table'))
          .fontSize(20)
          .margin(30)
          .fontWeight(FontWeight.Bold).onClick(() => {
          if (this.db) this.db.dropTable(TABLE_NAME);
        })
        Text($r('app.string.Delete_a_database'))
          .fontSize(20)
          .margin(30)
          .fontWeight(FontWeight.Bold).onClick(() => {
          if (this.db) this.db.dropDb();
        })
        Divider().margin({ top: 10 })
        Text($r('app.string.Load_an_image'))
          .fontSize(20)
          .margin(30)
          .fontWeight(FontWeight.Bold).onClick(() => {
          let config: BitmapDisplayConfig = new BitmapDisplayConfig();
          config.setLoadingPixMap(this.pixelMap)
          this.mBitmapUtils.display("https://scpic.chinaz.net/Files/pic/pic9/202109/apic35340_s.jpg", config, new MyBitmapLoadCallBack((pixMap) => {
            this.pixelMap = pixMap
          }))
        })
        Image(this.pixelMap==null ? $r('app.media.icon'):this.pixelMap)
          .width(200)
          .height(200)
          .border({ width: 1 })
          .margin(30)
          .borderStyle(BorderStyle.Dashed)
          .overlay('png', { align: Alignment.Bottom, offset: { x: 0, y: 20 } })
        Text($r('app.string.Retrieve_the_image_from_the_cache'))
          .fontSize(20)
          .margin(30)
          .fontWeight(FontWeight.Bold).onClick(() => {
          this.mBitmapUtils.display("https://scpic.chinaz.net/Files/pic/pic9/202109/apic35340_s.jpg", null, new MyBitmapLoadCallBack((pixMap) => {
            this.diskPixMap = pixMap
          }))
        })
        Image(this.diskPixMap==null ? $r('app.media.icon'):this.diskPixMap)
          .width(200)
          .height(200)
          .border({ width: 1 })
          .margin(30)
          .borderStyle(BorderStyle.Dashed)
          .overlay('png', { align: Alignment.Bottom, offset: { x: 0, y: 20 } })
        Button($r('app.string.Clear_local_files')).width(150).height(45).onClick((e: ClickEvent) => {
          this.mBitmapUtils.configBitmapCacheListener()
          this.mBitmapUtils.clearCache();
        }).margin(30)
      }
    }.scrollable(ScrollDirection.Vertical)
    .width('100%')
    .height('100%')
  }

  upload() {
    let path = this.context.cacheDir + '/file.txt'
    console.info("file path is " + path)
    let fd = fileIo.openSync(path, 0o100 | 0o2)
    let content: string = "hello, world123456"
    let num = fileIo.writeSync(fd.fd, content);
    fileIo.close(fd)
    console.info('num is ' + num)

    let header = new Map<string, Object>()
    header.set("Charset", "utf-8")
    header.set("Content-Type", "multipart/form-data")
    let uploadConfig: UploadBean = {
      url: 'http://pro-test.api.yesapi.cn/api/app.php?s=App.File.Upload',
      method: HttpMethod.POST,
      header: header,
      files: [{
                filename: 'file.txt',
                name: 'file',
                uri: 'internal://cache/file.txt',
                type: 'txt'
              } as FileBean],
      data: [
        {
          name: 'file1',
          value: 'file content',
        } as DataBean,
      ]
    }
    new HttpUtils().upload(uploadConfig, new UploadCallBack())
  }
}

/**
 * 网络请求回调
 * @param responseInfo
 */
class HttpCallBack<T extends Weather>  extends RequestCallBack<Weather> {
  onSuccess(responseInfo: ResponseInfo<Weather>) {
    console.log("HttpHandler：http request success : " + responseInfo.result)
    prompt.showToast({ message: 'http request success : ' + responseInfo.result })
  }

  onFailure() {
    console.log("HttpHandler：http request fail")
    prompt.showToast({ message: 'http request fail' })
  }

  onError(err: Object) {
    console.log("HttpHandler：http request error : " + JSON.stringify(err))
    prompt.showToast({ message: 'http request error ' + JSON.stringify(err) })
  }
  onDownloadSuccess(){

  }
  onDownloadProgress(receivedSize: number, totalSize: number){

  }

  onProgress(uploadedSize:number,totalSize:number){

  }

}

class HttpCallBackPost <T extends User> extends RequestCallBack<User> {
  onSuccess(responseInfo: ResponseInfo<User>) {
    console.log("HttpHandler：http request success : " + responseInfo.result)
    prompt.showToast({ message: 'http request success : ' + responseInfo.result })
  }

  onFailure() {
    console.log("HttpHandler：http request fail")
    prompt.showToast({ message: 'http request fail' })
  }

  onError(err: Object) {
    console.log("HttpHandler：http request error : " + JSON.stringify(err))
    prompt.showToast({ message: 'http request error : ' + JSON.stringify(err) })
  }
  onDownloadSuccess(){

  }
  onDownloadProgress(receivedSize: number, totalSize: number){

  }

  onProgress(uploadedSize:number,totalSize:number){

  }

}

class UploadCallBack <T extends User> extends RequestCallBack<User> {
  onSuccess(responseInfo: ResponseInfo<User>) {
    console.info("HttpHandler：upload success ")
    prompt.showToast({ message: 'upload task success' })
  }

  onFailure() {
    console.log("HttpHandler：http request fail")
    prompt.showToast({ message: 'http request fail' })
  }
  onDownloadProgress(receivedSize: number, totalSize: number) {
    console.log("HttpHandler: receivedSize=" + receivedSize + '  totalSize=' + totalSize);
  }

  onDownloadSuccess() {

  }

  onError(err: Object) {
    console.log("HttpHandler：upload error : " + JSON.stringify(err))
    prompt.showToast({ message: 'upload error : ' + JSON.stringify(err) })
  }


  onProgress(uploadedSize:number,totalSize:number) {
    console.info("HttpHandler：upload totalSize:" + totalSize + "--uploadedSize:" + uploadedSize)
  }
}


class DownloadCallBack <T extends User> extends RequestCallBack<User> {
  onSuccess(responseInfo: ResponseInfo<User>) {
    console.log("HttpHandler：http request success : " + responseInfo.result)
    prompt.showToast({ message: 'http request success : ' + responseInfo.result })
  }

  onFailure() {
    console.log("HttpHandler：http request fail")
    prompt.showToast({ message: 'http request fail' })
  }
  onDownloadProgress(receivedSize: number, totalSize: number) {
    console.log("HttpHandler: receivedSize=" + receivedSize + '  totalSize=' + totalSize);
  }

  onDownloadSuccess() {
    console.log("HttpHandler：download completed")
    prompt.showToast({ message: 'download task completed' })
  }

  onError(err: Object) {
    console.log("HttpHandler：download error : " + JSON.stringify(err))
    prompt.showToast({ message: 'download error : ' + JSON.stringify(err) })
  }


  onProgress(uploadedSize:number,totalSize:number){

  }
}

/**
 * 数据库查询回调
 * @param map
 */
class DbCallBack extends QueryCallBack {

  onSuccessCreateDb() {
    prompt.showToast({ message: getResourceString('Database_created') })
    console.log('DbUtils：create store done.')
  }

  onSuccessCreateTable() {
    prompt.showToast({ message: getResourceString('Table_created') })
    console.log('DbUtils：create table done.')
  }

  onSuccessInsert(ret: Object) {
    prompt.showToast({ message: getResourceString('Data_inserted') })
    console.log("DbUtils：insert done: " + ret)
  }

  onErrorInsert(err: Object) {
    console.log("DbUtils：insert err: " + JSON.stringify(err))
    prompt.showToast({ message: getResourceString('Insert_Failed') })
  }

  onSuccessUpdate() {
    console.log("DbUtils：update done")
    prompt.showToast({ message: getResourceString('Data_updated') })
  }

  onErrorUpdate(err: Object) {
    console.log("DbUtils：update err: " + JSON.stringify(err))
    prompt.showToast({ message: getResourceString('Update_failed') })
  }

  onSuccessDelete(rows: Object) {
    console.log("DbUtils：delete done " + rows)
    prompt.showToast({ message: getResourceString('Data_deleted') })
  }

  onErrorDelete(err: object) {
    console.log("DbUtils：delete err: " + JSON.stringify(err))
    prompt.showToast({ message: getResourceString('Delete_failed') })
  }

  onSuccessQuery(resultSet: ESObject) {
    prompt.showToast({ message: resultSet.rowCount + "data records obtained" })
  }

  onFailQuery(msg: string){
    prompt.showToast({ message: msg })
  }

  onSuccessQueryFirst(map: Map<string, object>) {
    prompt.showToast({ message: getResourceString('Query_successful') })
    console.log("DbUtils： this.personEntity.AGE=" + map.get("AGE"))
    console.log("DbUtils： this.personEntity.SALARY=" + map.get("SALARY"))
  }

  onSuccessDropTable() {
    console.log("DbUtils：drop table done ")
    prompt.showToast({ message: getResourceString('Table_deleted') })
  }

  onSuccessDeleteDb() {
    console.log('DbUtils：delete db success')
    prompt.showToast({ message: getResourceString('Database_deleted') })
  }
}

class MyBitmapLoadCallBack extends BitmapLoadCallBack {
  MyCallBackFun: ESObject = null

  constructor(fuc: (pixMap: PixelMap) => void) {
    super()
    this.MyCallBackFun = fuc
  }

  onLoadCompleted(uri: string, bitmap: PixelMap) {
    this.MyCallBackFun(bitmap)
    bitmap.getImageInfo((info)=>{
      console.info("BitmapLoadCallBack complete:"+JSON.stringify(info))
    })
    return bitmap;
  }

  onLoadDiskCompleted(uri: string, bitmap: PixelMap): PixelMap{
    return bitmap;
  }

  onLoadFailedPixelMap(value ?: PixelMap) {
    return value;
  }
  onLoadFailedResource(Resource: Resource){

  }
  onLoadMemoryCompleted(url: string, pixelMap: PixelMap) {
    this.MyCallBackFun(pixelMap)
    return pixelMap;
  }

  onProgressUpdate(receivedSize: number, totalSize: number) {
    console.log("BitmapUtils onProgressUpdate receivedSize : " + receivedSize + "receivedSize :" + totalSize)
  }

  onLoadingPixelMap(PixelMap: PixelMap) {
    console.log("BitmapUtils onLoadingPixelMap PixelMap "+(PixelMap == null))
  }

  onLoadStarted(disconfig: ESObject) {
    console.log("BitmapUtils onLoadStarted PixelMap : "+JSON.stringify(disconfig))
  }

}

function getResourceString(resourceName:string) {
  return getContext().resourceManager.getStringByNameSync(resourceName)
}